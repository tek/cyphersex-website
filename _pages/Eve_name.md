---
layout: single
permalink: /Eve_name/
title: Eve Pentest - Choosing a Name
header:
  overlay_color: "#000"
  overlay_filter: "0.3"
  overlay_image: /assets/images/guide/1.jpg
  actions:
    - label: "Read the full guide"
      url: /Eve_Pentest/
excerpt: "Eve Pentest is a digital security dominatrix who knows how to secure boundaries and create a safer space to play. Eve knows a lot about technology and how the internet operates. She uses many online services for her work and knows how to protect herself against potential stalkers, hackers and haters. She works in a dedicated dungeon and keeps her private life completely separated from her work life."
---

# Choosing a Name

Like many pro dommes, Eve has chosen a work name that sounds cool, but on commercial
social networking platforms her username is Eve "Pentest" Adams. She has added
a real-sounding surname to her work name and, although this sounds boring, there is
an important reason.

![](../assets/images/guide/1.jpg){:height="300px" width="300px"}

On the internet, platforms that have "real name" policies (like Facebook) tend to base this judgement
on an individual's legal name, rather than allowing them to identify as they choose.
Many companies require both a first name and surname for registration (or a name
that doesn't contain any slang terms or profanities), so Eve has added a common
surname to her work name. Ultimately, this won't protect her if she
falls under the radar of the "real name" policy enforcers, but automated controls
won't spot her name as a potential violation and she can hope to keep her account
for some time.

Once she decided on a name, a surname, and a username for her work persona, Eve
also did thorough research on various sex work platforms, as well as following
[this guide](https://guides.accessnow.org/self-doxing.html),
so she is sure that nobody else is using that name, at least among sex workers
in her city.
