---
layout: single
permalink: /Eve_Clients_Communications/
title: Eve Pentest - Staying in Touch with Clients
header:
  overlay_color: "#000"
  overlay_filter: "0.3"
  overlay_image: /assets/images/guide/12.jpg
  actions:
    - label: "Read the full guide"
      url: /Eve_Pentest/
excerpt: "Eve Pentest is a digital security dominatrix who knows how to secure boundaries and create a safer space to play. Eve knows a lot about technology and how the internet operates. She uses many online services for her work and knows how to protect herself against potential stalkers, hackers and haters. She works in a dedicated dungeon and keeps her private life completely separated from her work life."
---

# Communicate with Clients

## Staying in Touch with Clients

Once she has decided to arrange a session with a new client, she asks them to
establish a more secure contact than email. In her confirmation email she writes:


![](../assets/images/guide/12.jpg){:height="500px" width="500px"}

> Hi dear,
>
> I would be happy to make plans for a session, but first it would be good to
> establish a more trusted communication channel, both for your and my safety.
> We can keep talking on one of the following platforms - just let me know which
> one you prefer:
>
> - If you want to keep using emails, you can create an account on Protonmail: https://protonmail.com
>   Once you've set it up, you can contact me on this address from that account.
> - If you prefer to use a phone, we can use one of the following tools:
>     - Wire - https://app.wire.com - you can create an account with your computer,
>       and then install the app in your phone, logging in with the account you
>       created.
>     - Signal - https://signal.org/ - this is also a good solution if you don't
>       mind using your phone number. Of course, you could also get a different
>       phone number to create an account.
> Let me know,
> Eve

If the client chooses Wire or Signal, Eve sets messages to disappear within 1 day,
so even if she or the client loses their device, the messages can't be seen by
anybody else, as they will have already disappeared.

- [Setting disappearing messages on Wire](https://support.wire.com/hc/en-us/articles/213216845-How-do-Timed-Messages-work-)
- [Setting disappearing messages on Signal](https://support.signal.org/hc/en-us/articles/360007320771-Set-and-manage-disappearing-messages)
