---
layout: single
permalink: /Eve_Email/
title: Eve Pentest - Tips on Email
header:
  overlay_color: "#000"
  overlay_filter: "0.3"
  overlay_image: /assets/images/guide/email_cut.jpg
  actions:
    - label: "Read the full guide"
      url: /Eve_Pentest/
excerpt: "Eve Pentest is a digital security dominatrix who knows how to secure boundaries and create a safer space to play. Eve knows a lot about technology and how the internet operates. She uses many online services for her work and knows how to protect herself against potential stalkers, hackers and haters. She works in a dedicated dungeon and keeps her private life completely separated from her work life."
---

# Eve's Tips on Email

When she started looking for an email provider that would be good to use for work,
Eve already knew that there is no such thing as a secure email. By default,
emails are not encrypted and if someone can access the servers where messages
are stored (often over several machines belonging to both the sender's and receiver's email
provider), they can read everything. This includes investigators, but also a random
system administrator who has access to these machines.

![](../assets/images/guide/9.jpg){:height="300px" width="300px"}

Eve knew that she would not use her email for any sensitive content, but she
still wanted to make sure that her email could not be tapped while in transit. So,
she only chose among email providers that use HTTPS/TLS encryption, which
encrypts connections from her computer to the servers, and she made sure the
TLS encryption was actually working by testing the provider she'd chosen on [this webpage](https://www.checktls.com/).

[This is the list of email providers Eve chose from.](http://prxbx.com/email/)

To be contacted by her clients, Eve has a Protonmail account which does encrypt
her messages with other Protonmail users all along the way so messages are not
readable in the servers. However, she doesn't consider this encryption reliable enough
because emails with people who do not have a mailbox on Protonmail are not encrypted.


### Separate Email Accounts

To keep things really separated, Eve has a different email account for each of her
separate identities and has created different online accounts, according to her
different needs, with that corresponding email address.

For her work, she has created a [Protonmail](https://protonmail.com/) account
so encrypting communications with her clients becomes easier.

Sometimes Eve just needs to log into a service once so she creates a temporary
account with a disposable email address like the ones offered by [Guerrilla Mail](https://www.guerrillamail.com/)
or [anonbox](https://anonbox.net).
