---
layout: single
permalink: /Eve_Accounts/
title: Eve Pentest - Online Accounts
header:
  overlay_color: "#000"
  overlay_filter: "0.3"
  overlay_image: /assets/images/guide/saintandrewcut.jpg
  actions:
    - label: "Read the full guide"
      url: /Eve_Pentest/
excerpt: "Eve Pentest is a digital security dominatrix who knows how to secure boundaries and create a safer space to play. Eve knows a lot about technology and how the internet operates. She uses many online services for her work and knows how to protect herself against potential stalkers, hackers and haters. She works in a dedicated dungeon and keeps her private life completely separated from her work life."
---

# Eve's Online Accounts

To advertise her services, Eve uses both mainstream social networking platforms
and specialized platforms for BDSM communities and sex work. At the same time,
she uses different accounts on almost all these websites to also communicate
with her family, friends, and lovers, but, of course, she uses different accounts
for each of them.

![](../assets/images/guide/saintandrew.jpg){:height="300px" width="300px"}

She has the following accounts:

| Platform  | Contacts | Real or Fake Name | Level of Trust | NSFW? | Disposable Account |
|-----------|----------|----------------   |----------------|------|--------------------|
| Facebook  | Friends  |  Realistic Fake Name 1 |  High     |  Yes |    Yes             |
| Facebook  | Family   |  Real Name        |  Medium        |  No  |    No              |
| Instagram | Friends  |  Realistic Fake Name 2 |  High     |  Yes |    Yes             |
| Instagram | Work     |  Work Name        |  Low           |  OFC! |   Yes             |
| Twitter   | Public   |  Work Name        |  Low           |  Yes |    No              |
| Fetlife   | Friends and lovers | Fake Name 2 |  High      |  OFC! |   No              |
| Fetlife   | Work     |  Work Name        |  Low           |  OFC! |   Yes             |
| onlyfans <br /> niteflirt <br /> eros | Work | Work Name | Low | OFC! | No            |
| Switter.at | Public  | Work Name         |  High          | Yes  |    No              |

Eve considers all accounts on services that require to use official names, or
restrict adult content or sex work (see ["Terms of Use"](#terms-of-use) below),
as disposable. She tries to use a realistic name on Facebook, to avoid being
spotted by Facebook's bots that try to identify fake names, but knows that at
some point her account might be suspended and she would be asked for an ID to recover it.

To keep her accounts really separated, she avoids connecting them to her real identity unless she's decided to use her
official identity in the first place (as she does with her family, or her bank -
who both know it anyway). Additionally, she keeps more stable accounts where one can always
find her such as on dedicated platforms like [Switter](https://switter.at/about) (a
sex-worker friendly social space) or commercial sex work platforms, and tells
her contacts that they can find her there if her other accounts are suddenly deleted or blocked.

However, Eve never takes for granted that these accounts will last
very long and keeps a backup of everything she wants to keep in her local machine and an external hard drive.


### Isolated Accounts

To keep her accounts isolated, and be sure that they cannot be connected to each
other, Eve follows these additional rules:

- She only manages her work accounts from her work device.
- She doesn't follow the same people from different accounts connected to different
identities.
- She is careful never to befriend one of her identities with another separate
identity, and never to post the same content with different identities.
- Eve knows that most social networking platforms will display her location
whenever they can, so she disables geolocation in her phone apps, and activates
the GPS in her devices only when she really needs it.
- Eve also knows that many apps and cameras will embed metadata into her photos,
which can include the date, time and location of the photo among other things.
This metadata may be included in the pictures and videos she shares online, so
she always checks that geolocation is disabled when she takes pictures and shoots
videos.
