---
layout: single
permalink: /Eve_Pictures/
title: Eve Pentest - Dedicated Pictures
header:
  overlay_color: "#000"
  overlay_filter: "0.3"
  overlay_image: /assets/images/guide/inflate_here.jpg
  actions:
    - label: "Read the full guide"
      url: /Eve_Pentest/
excerpt: "Eve Pentest is a digital security dominatrix who knows how to secure boundaries and create a safer space to play. Eve knows a lot about technology and how the internet operates. She uses many online services for her work and knows how to protect herself against potential stalkers, hackers and haters. She works in a dedicated dungeon and keeps her private life completely separated from her work life."
---

# Dedicated Pictures

![](../assets/images/guide/inflate_here.jpg){:height="300px" width="300px"}

Most importantly, Eve never re-uses personal photos for work. She knows that many search
engines offer reverse image search functionalities that can identify all the
places where a picture was published. To avoid someone connecting her work
identity with other identities through a reverse image search, she never uses
pictures she has published in other accounts on her work profiles or when
communicating with her clients.
